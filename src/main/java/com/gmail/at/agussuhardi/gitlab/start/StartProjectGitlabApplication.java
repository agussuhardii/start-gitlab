package com.gmail.at.agussuhardi.gitlab.start;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StartProjectGitlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(StartProjectGitlabApplication.class, args);
	}
}
